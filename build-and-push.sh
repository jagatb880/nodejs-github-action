#!/bin/bash
# Set variables
IMAGE_TAG=latest
REPOSITORY=ngapro-app
GITHUB_OUTPUT=output.txt
ECR_REGISTRY="708287751177.dkr.ecr.us-east-1.amazonaws.com/ngapro-app"

# Build the Docker container
docker build -t $ECR_REGISTRY/$REPOSITORY:$IMAGE_TAG .

# Push the container to ECR
docker push $ECR_REGISTRY/$REPOSITORY:$IMAGE_TAG

# Output the image tag for later use
echo "image=$ECR_REGISTRY/$REPOSITORY:$IMAGE_TAG" >> $GITHUB_OUTPUT

# Optional: Add any other actions you need after the push
# (Replace the placeholder with your actual logic)
# magic___^_^___line